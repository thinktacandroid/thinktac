package com.thinktac.thinktac;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions.Builder;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;
import com.thinktac.thinktac.model.Program;

public class HomeFragment extends Fragment {
    private ImageView imageViewScan;
    private CardView cardView;
    private FirebaseListAdapter listAdapter;
    private ListView listView;
    private String pgmID;
    private int pos = 0;
    private Program program;
    private TextView textView;
    ProgressBar homeProgressBar;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        this.listView = (ListView) view.findViewById(R.id.listViewPgm);
        imageViewScan = view.findViewById(R.id.scanView);
        homeProgressBar=view.findViewById(R.id.homeProgressBar);
        homeProgressBar.setVisibility(View.VISIBLE);
        imageViewScan.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ScanActivity.class);
                startActivity(intent);
            }
        });


        this.listAdapter = new FirebaseListAdapter<Program>(new Builder().setLayout(R.layout.activity_program)
                .setQuery(FirebaseDatabase.getInstance().
                        getReferenceFromUrl("https://think-tac-india.firebaseio.com/")
                        .child("programs"), Program.class).build()) {
            protected void populateView(View v, final Program model, int position) {
                HomeFragment.this.cardView = (CardView) v.findViewById(R.id.cardViewPgm);
                ImageView imageView = (ImageView) v.findViewById(R.id.imageViewPgm);
                HomeFragment.this.textView = (TextView) v.findViewById(R.id.textViewPgm);
                Picasso.get().load(model.getImage()).into(imageView);
                HomeFragment.this.textView.setText(model.getName());


                homeProgressBar.setVisibility(View.INVISIBLE);
                HomeFragment.this.cardView.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        Intent i = new Intent(HomeFragment.this.getActivity(), TactivityList.class);
                        String selectedName = ((TextView) view.findViewById(R.id.textViewPgm)).getText().toString();
                        String programId = model.getPid();
                        i.putExtra("PgmName", selectedName);
                        i.putExtra("pgmId", programId);

                        HomeFragment.this.startActivity(i);
                    }
                });
            }
        };
        this.listView.setAdapter(this.listAdapter);
        return view;
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void onStart() {
        super.onStart();
        this.listAdapter.startListening();
        }

    public void onResume() {
        super.onResume();

    }

    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();
        this.listAdapter.stopListening();
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onDetach() {
        super.onDetach();
    }
}
