package com.thinktac.thinktac.model;

import android.support.annotation.Keep;

@Keep
public class Tactivity {
    public String guideUrl;
    public String name;
    public String videoUrl;
    public String forums;

    public Tactivity(String guideUrl, String name, String videoUrl, String forums) {
        this.guideUrl = guideUrl;
        this.name = name;
        this.videoUrl = videoUrl;
        this.forums = forums;
    }

    public Tactivity() {
    }

    public String getGuideUrl() {
        return guideUrl;
    }

    public void setGuideUrl(String guideUrl) {
        this.guideUrl = guideUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getForums() {
        return forums;
    }

    public void setForums(String forums) {
        this.forums = forums;
    }
}
