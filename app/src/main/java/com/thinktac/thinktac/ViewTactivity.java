package com.thinktac.thinktac;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.thinktac.thinktac.model.Tactivity;
import com.thinktac.thinktac.model.YoutubePlayer;

import java.util.ArrayList;
import java.util.List;

public class ViewTactivity extends YouTubeBaseActivity {

    YoutubePlayer youtubePlayer;
    private Tactivity tac;
    private String TID;
    private List<Tactivity> tactivityList;
    private TextView tv_result;
    private FirebaseDatabase firebaseDatabase;
    private TextView textViewName;
    private ProgressDialog progressDialog;
    private String videoUrl;
    private TextView playTextView;
    private CardView pdfButton;
    private CardView buttonUpload;
    private CardView playQuiz;
    private CardView forumButton;
    private String errorMessage;
    YouTubePlayerView youTubePlayerView;
    private static final int GALLERY_INTENT=2;
    YouTubePlayer.OnInitializedListener onInitializedListener;
    private DatabaseReference databaseReference;
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_tactivity);
        forumButton = findViewById(R.id.cardForum);

        playTextView = findViewById(R.id.playVideoText);
        progressDialog = new ProgressDialog(this);
        textViewName = findViewById(R.id.textViewName);
        youTubePlayerView = findViewById(R.id.youTubeView);
        //buttonUpload = findViewById(R.id.cardUpload);
        pdfButton = findViewById(R.id.cardRead);
        firebaseDatabase = FirebaseDatabase.getInstance();

        //Bundle bundle = getIntent().getExtras();
        //TID="CC14";
        Intent intent=getIntent();

//        if(bundle!=null){
//          TID = bundle.getString("TID");
//        }
        if (intent!=null){
            TID=intent.getStringExtra("TID");
        }


        final DatabaseReference databaseReference = firebaseDatabase.getReferenceFromUrl("https://think-tac-india.firebaseio.com/tactivities/")
                .child(TID);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                tac = dataSnapshot.getValue(Tactivity.class);
                textViewName.setText(tac.getName());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        pdfButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ReadGuide.class);
                intent.putExtra("guide",tac.getGuideUrl());
                startActivity(intent);
            }
        });

       // textViewName.setText(tac.getName());

        forumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String forumUrl = tac.getForums();
                Intent intent = new Intent(getApplicationContext(),ForumsActivity.class);
                intent.putExtra("url",forumUrl);
                startActivity(intent);
            }
        });

        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                if(!b){
                    youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);

                    youTubePlayer.loadVideo(tac.getVideoUrl());

                    youTubePlayer.play();
                    playTextView.setVisibility(View.INVISIBLE);
                }


            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                errorMessage = youTubeInitializationResult.toString();
                Toast.makeText(getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();
            }

        };




    }

    public void playVideo(View view) {
        youTubePlayerView.initialize("AIzaSyAsp3rzRdZZFQ5BdPWHSYRzVBkjuw-MK4A",onInitializedListener);
    }
}
