package com.thinktac.thinktac;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Intent startSignedinIntent;
    private Button scanButton;
    private Button RegisterBtn;
    private static final int RC_SIGN_IN = 123;
    private Dialog registerDialog;
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        auth = FirebaseAuth.getInstance();
        startSignedinIntent = new Intent(MainActivity.this,SignedInActivity.class);



        //check whether user has already signed in
        if(auth.getCurrentUser() != null) {
            try{
               if(auth.getUid()!=null) {
                     startSignedinIntent.putExtra("userId", auth.getUid());
                     startSignedinIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                     startSignedinIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                     startActivity(startSignedinIntent);
                    }
           }
           catch (NullPointerException e)
           {
               e.printStackTrace();
           }

        }



        RegisterBtn = findViewById(R.id.registerBtn);
        scanButton = findViewById(R.id.scanBtn);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,ScanActivity.class);
                startActivity(intent);
            }
        });


        //  Declare a new thread to do a preference check
        Thread introThread = new Thread(new Runnable() {
            @Override
            public void run() {

                //  Initialize SharedPreferences
                SharedPreferences getPrefs = PreferenceManager
                        .getDefaultSharedPreferences(getBaseContext());


                //  Create a new boolean and preference and set it to true
                boolean isFirstStart = getPrefs.getBoolean("firstStart", true);

                //  If the activity has never started before...
                if (isFirstStart) {

                    //  Launch app intro
                    final Intent i = new Intent(MainActivity.this, IntroActivity.class);

                    runOnUiThread(new Runnable() {
                        @Override public void run() {
                            startActivity(i);
                        }
                    });

                    //  Make a new preferences editor
                    SharedPreferences.Editor e = getPrefs.edit();

                    //  Edit preference to make it false because we don't want this to run again
                    e.putBoolean("firstStart", false);

                    //  Apply changes
                    e.apply();
                }
            }
        });

        introThread.start();





        RegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callRegisterDialog();
            }

            private void callRegisterDialog() {
                registerDialog = new Dialog(getApplicationContext());
                registerDialog.setCancelable(false);

                List<AuthUI.IdpConfig> providers = Arrays.asList(
                        new AuthUI.IdpConfig.EmailBuilder().build(),
                        new AuthUI.IdpConfig.PhoneBuilder().setDefaultCountryIso("in").build(),
                        new AuthUI.IdpConfig.GoogleBuilder().build());
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setAvailableProviders(providers)
                                    .setLogo(R.drawable.thinktac_logo)
                                    .setTheme(R.style.Theme_AppCompat_DayNight_NoActionBar)
                                    .setIsSmartLockEnabled(false)
                                    .build(),
                            RC_SIGN_IN);
            }
        });




    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            // Successfully signed in
            if (resultCode == RESULT_OK) {
                startSignedinIntent.putExtra("userId",auth.getUid());
                startSignedinIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startSignedinIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(startSignedinIntent);
                finish();
            } else {
                // Sign in failed
                if (response == null) {
                    // User pressed back button
                    Toast.makeText(getApplicationContext(),"LogIn failed",Toast.LENGTH_SHORT).show();
                    return;
                }

                if (response.getError().getErrorCode() == ErrorCodes.NO_NETWORK) {
                    Toast.makeText(getApplicationContext(),"Please check your internet connection!",Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(getApplicationContext(),"Sign-in error:"+response.getError(),Toast.LENGTH_SHORT).show();

            }
        }
    }
}
