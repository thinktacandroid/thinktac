package com.thinktac.thinktac.model;

import android.support.annotation.Keep;

/**
 * Created by Pratik on 9/17/2018.
 */
@Keep
public class ScanData {
    public String url;
    public String taccode;


    public ScanData() {
    }

    public ScanData(String url, String taccode) {
        this.url = url;
        this.taccode = taccode;
    }

    public String getUrl() {
        return url;
    }

    public String getTaccode() {
        return taccode;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setTaccode(String taccode) {
        this.taccode = taccode;
    }




}
