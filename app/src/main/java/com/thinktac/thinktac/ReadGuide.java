package com.thinktac.thinktac;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ReadGuide extends AppCompatActivity {

    ProgressBar guideProgressBar;
    com.github.barteksc.pdfviewer.PDFView pdfView;
    private String Guide;
    private PDFView ReadpdfView;

        @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_read_guide);

            guideProgressBar=findViewById(R.id.guideProgressBar);
            ReadpdfView=findViewById(R.id.pdfView);
            checkConnection();


            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                Guide = bundle.getString("guide");

            }


        }

        class RetrievePDFSTream  extends AsyncTask<String,Void,InputStream>
        {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                guideProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(InputStream inputStream) {
                if (inputStream!=null) {

                    ReadpdfView.fromStream(inputStream).load();
                    guideProgressBar.setVisibility(View.INVISIBLE);

                }
                else {
                    Toast.makeText(getApplicationContext(),"null raised",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            protected InputStream doInBackground(String... strings) {
                InputStream inputStream=null;


                try
                {
                    URL url=new URL(Guide);
                    HttpURLConnection httpURLConnection=(HttpURLConnection) url.openConnection();
                    if (httpURLConnection.getResponseCode() == 200){
                        inputStream=new BufferedInputStream(httpURLConnection.getInputStream());
                    }
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                    return null;
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }

                return inputStream;
            }
        }


    protected boolean isOnline() {

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnectedOrConnecting()) {

            return true;

        } else {

            return false;

        }

    }

    public void checkConnection(){

        if(isOnline()){
            new RetrievePDFSTream().execute(Guide);
        }else{

            Toast.makeText(getApplicationContext(), "Please check the internet connection!", Toast.LENGTH_SHORT).show();

        }

    }
    }
