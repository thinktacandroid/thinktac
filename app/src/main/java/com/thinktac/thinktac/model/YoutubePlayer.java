package com.thinktac.thinktac.model;

import android.support.annotation.Keep;

@Keep
public class YoutubePlayer {

    public String API_KEY = "AIzaSyAsp3rzRdZZFQ5BdPWHSYRzVBkjuw-MK4A";

    public String getAPI_KEY() {
        return API_KEY;
    }

    public void setAPI_KEY(String API_KEY) {
        this.API_KEY = API_KEY;
    }

    public YoutubePlayer() {

    }

    public YoutubePlayer(String API_KEY) {

        this.API_KEY = API_KEY;
    }
}
