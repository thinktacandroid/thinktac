package com.thinktac.thinktac;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;
import com.github.vipulasri.timelineview.TimelineView;
import com.thinktac.thinktac.model.Recycler_item_model;
import com.thinktac.thinktac.utils.VectorDrawableUtils;
import java.util.List;

public class RecyclerAdapter extends Adapter<RecyclerViewHolder> {
    private Context context;
    private List<Recycler_item_model> recycler_item_models_list;

    public RecyclerAdapter(List<Recycler_item_model> recycler_item_models_list, Context context) {
        this.recycler_item_models_list = recycler_item_models_list;
        this.context = context;
    }

    @NonNull
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new RecyclerViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item, viewGroup, false), i);
    }

    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final Recycler_item_model recycler_item_model = (Recycler_item_model) this.recycler_item_models_list.get(position);
        holder.tvTactivityName.setText(recycler_item_model.getName());
        if (recycler_item_model.getStatus() == 0) {
            holder.timelineView.setMarker(VectorDrawableUtils.getDrawable(this.context, R.drawable.ic_marker_inactive, 17170432));
        } else if (recycler_item_model.getStatus() == 1) {
            holder.timelineView.setMarker(VectorDrawableUtils.getDrawable(this.context, R.drawable.ic_marker_active, R.color.colorPrimary));
        } else {
            holder.timelineView.setMarker(VectorDrawableUtils.getDrawable(this.context, R.drawable.ic_marker, R.color.colorPrimary));
        }
        holder.linearLayout_tactivities_list.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context.getApplicationContext(),ViewTactivity.class);
                intent.putExtra("TID",recycler_item_model.getTID());
                context.startActivity(intent);
            }
        });
    }

    public int getItemCount() {
        return this.recycler_item_models_list.size();
    }

    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }
}
