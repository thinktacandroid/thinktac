package com.thinktac.thinktac;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.LogPrinter;
import android.util.SparseArray;
import android.widget.Toast;
import android.util.Log;

import com.google.android.gms.vision.barcode.Barcode;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.thinktac.thinktac.model.ScanData;

import java.util.List;

import info.androidhive.barcode.BarcodeReader;

public class ScanActivity extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener{

    BarcodeReader barcodeReader;
    String barcodeString;
    private String retrieveUrl;

    private FirebaseDatabase firebaseDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        barcodeReader= (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_scanner);

        firebaseDatabase=FirebaseDatabase.getInstance();

    }

    @Override
    public void onScanned(Barcode barcode) {


        barcodeReader.playBeep();




        barcodeString=barcode.displayValue.toString();

       Log.i("barcodeout",barcodeString);

        final DatabaseReference databaseReference=firebaseDatabase.getReference("/scandata");
//
//        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                ScanData scanData=dataSnapshot.getValue(ScanData.class);
//                Log.i("retrieveUrl",scanData.getUrl()+" url");
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });


        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                   ScanData data = snapshot.getValue(ScanData.class);
                   retrieveUrl = data.getUrl();
                   Log.i("barcode",barcodeString);
                   Log.i("retreive",data.getUrl()+" Url");
                   if(retrieveUrl.trim().equalsIgnoreCase(barcodeString.trim())) {

                       callScan(data.getTaccode(), retrieveUrl);
                       break;
                   }else{
                       finish();
                   }

               }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void callScan(String taccode, String retrieveUrl) {
        if(retrieveUrl.equalsIgnoreCase(barcodeString)){
            Intent newIntent = new Intent(getApplicationContext(),ViewTactivity.class);
            newIntent.putExtra("TID",taccode);
            startActivity(newIntent);
        }

    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {

        Toast.makeText(getApplicationContext(),R.string.scanning_Error,Toast.LENGTH_LONG).show();

    }

    @Override
    public void onCameraPermissionDenied() {
        Toast.makeText(getApplicationContext(),R.string.camera_permission_denied,Toast.LENGTH_LONG).show();

    }
}
