package com.thinktac.thinktac;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.github.vipulasri.timelineview.TimelineView;

public class RecyclerViewHolder extends RecyclerView.ViewHolder {
    public LinearLayout linearLayout_tactivities_list;
    public TimelineView timelineView;
    public TextView tvTactivityName;

    public RecyclerViewHolder(View itemView, int viewType) {
        super(itemView);
        this.timelineView = (TimelineView) itemView.findViewById(R.id.time_marker);
        this.tvTactivityName = (TextView) itemView.findViewById(R.id.TvTactivityName);
        this.linearLayout_tactivities_list = (LinearLayout) itemView.findViewById(R.id.Linearlayout_tactivities);
        this.timelineView.initLine(viewType);
    }
}
