package com.thinktac.thinktac;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.thinktac.thinktac.model.User;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Pattern;

public class UserDetails extends AppCompatActivity {

    private TextView firstName, lastName;
    private ImageButton calendarButton;
    private DatabaseReference mDatabase;
    private Context mContext;
    private Uri mainimageUri = null;
    private static final int GALLERY_INTENT = 2;
    private Boolean imageSet;
    private Boolean dataSet = false;
    private EditText editTextFirst;
    private EditText editTextLast;
    private EditText calendarEdit;
    private EditText editMobile;
    private EditText editTextEmail;
    SharedPreferences.Editor editor;
    private Button signOutButton;


    private ImageView circleImageView;
    private String user_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);


        circleImageView = findViewById(R.id.cirImageView);
        editTextFirst = findViewById(R.id.editTextFirstName);

        calendarEdit = findViewById(R.id.calendarEdit);
        editMobile = findViewById(R.id.editMobileNo);
        editTextEmail = findViewById(R.id.editEmailId);
        imageSet = false;
        calendarButton = findViewById(R.id.calendarButton);
        signOutButton = findViewById(R.id.signOutButton);


        Button button = findViewById(R.id.saveButton);



        final Calendar myCalendar = Calendar.getInstance();
        final Calendar calendar = Calendar.getInstance();
        final int mYear = calendar.get(Calendar.YEAR);
        final int mMonth = calendar.get(Calendar.MONTH);
        final int mDay = calendar.get(Calendar.DAY_OF_MONTH);


        final DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                myCalendar.set(Calendar.YEAR, i);
                myCalendar.set(Calendar.MONTH, i1);
                myCalendar.set(Calendar.DAY_OF_MONTH, i2);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "dd/MM/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                calendarEdit.setText(sdf.format(myCalendar.getTime()));
            }
        };





        calendarButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                DatePickerDialog dpDialog = new DatePickerDialog(UserDetails.this, myDateListener, mYear, mMonth, mDay);
                dpDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                dpDialog.show();
            }
        });

// ...
        mDatabase = FirebaseDatabase.getInstance().getReference();


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // Name, email address, and profile photo Url
            String name = user.getDisplayName();
            String email = user.getEmail();
            Uri photoUrl = user.getPhotoUrl();

            String mobileNo = user.getPhoneNumber();
            editTextFirst.setText(name);
            editTextEmail.setText(email);

            if(editTextEmail.getText().toString().length() <= 0 ){
                editMobile.setEnabled(false);

            }
            editMobile.setText(mobileNo);
            if(editMobile.getText().toString().length() <= 0 ){
                editTextEmail.setEnabled(false);

            }

            // Check if user's email is verified
            boolean emailVerified = user.isEmailVerified();

            // The user's ID, unique to the Firebase project. Do NOT use this value to
            // authenticate with your backend server, if you have one. Use
            // FirebaseUser.getIdToken() instead.
            String uid = user.getUid();
        }


        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(UserDetails.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                    } else {
                        CropImage.activity()
                                .setGuidelines(CropImageView.Guidelines.ON)
                                .start(UserDetails.this);
                    }
                }

            }


        });




        //button.setEnabled(false);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextFirst.getText().toString().length() <= 0 || calendarEdit.getText().toString().length() <= 0 ||
                        editTextEmail.getText().toString().length() <= 0 || editMobile.getText().toString().length() <= 0) {
                    Toast.makeText(getApplicationContext(), "Please fill in all the fields", Toast.LENGTH_SHORT).show();

                } else {

                    // Create a storage reference from our app
                    // Create a storage reference from our app

                    final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                    if (user != null) {
                        user_id = user.getUid();
                    }
                    final FirebaseStorage storage = FirebaseStorage.getInstance();
                    final StorageReference img_path = storage.getReference().child("profile_images").child(user_id + ".jpg");
                    if (mainimageUri != null) {
                        img_path.putFile(mainimageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                if (task.isSuccessful()) {
                                    imageSet = true;
                                    mDatabase.child("users_metadata").child(user_id).child("imageSet").setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){

                                                Toast.makeText(getApplicationContext(), "Photo Uploaded Successfully", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                } else {

                                    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                                }

                            }

                        });
                    }
                    String FName = editTextFirst.getText().toString();
                    String MobileNo = editMobile.getText().toString();
                    String email = editTextEmail.getText().toString();
                    String DOB = calendarEdit.getText().toString();

                    if(isValidMail(email) && isValidMobile(MobileNo )){
                        writeNewUser(FName, email, DOB, MobileNo,imageSet);
                    }else if(!isValidMail(email)){
                        Toast.makeText(getApplicationContext(),"Enter valid Email address",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(),"Enter valid Mobile Number",Toast.LENGTH_SHORT).show();

                    }

                }
            }

        });

    }






    private void writeNewUser(String firstName, String email, String DOB, String MobileNo, final Boolean imageSet) {
        User users = new User(firstName, email, DOB, MobileNo,imageSet);

        mDatabase.child("users_metadata").child(user_id).setValue(users).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                    Toast.makeText(getApplicationContext(), "Your profile Details is updated", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Error while uploading" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mainimageUri = result.getUri();
                circleImageView.setImageURI(mainimageUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

    }

    private boolean isValidMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isValidMobile(String phone) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            if(phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
                Toast.makeText(getApplicationContext(),"Not a valid Number",Toast.LENGTH_SHORT).show();
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }
}

