package com.thinktac.thinktac;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

public class ForumsActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private int progress = 0;

    WebView webView;
    private  String forumLink;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forums);
        webView = findViewById(R.id.webView);

        checkConnection();
    }

    protected boolean isOnline() {

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnectedOrConnecting()) {

            return true;

        } else {

            return false;

        }

    }

    public void checkConnection(){

        if(isOnline()){
            loadPage();
        }else{

            Toast.makeText  (getApplicationContext(), "Please check the internet connection!", Toast.LENGTH_SHORT).show();

        }

    }

    private void loadPage() {
        Bundle bundle = getIntent().getExtras();

        if(bundle!=null){
            forumLink = bundle.getString("url");
            webView.loadUrl(forumLink);
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
            webView.setWebViewClient(new WebViewClient());
            webView.setWebChromeClient(new WebChromeClient());
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        }
    }

}
