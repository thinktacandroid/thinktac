package com.thinktac.thinktac;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.thinktac.thinktac.model.Recycler_item_model;

import java.util.ArrayList;
import java.util.List;

public class TactivityList extends AppCompatActivity {
    private String PID;
    private Adapter adapter;
    private FirebaseDatabase firebaseDatabase;
    private String pgmName;
    private RecyclerView recyclerView;
    private List<Recycler_item_model> recycler_item_models_main_list;
    private Toolbar toolbar;
    private TextView textView;

    /* renamed rom: com.thinktac.thinktac.TactivityList$1 */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tactivity_list_layout);
//        toolbar = findViewById(R.id.tabs);
        textView = findViewById(R.id.pgmName);

        recyclerView = findViewById(R.id.recyclerView);
        firebaseDatabase = FirebaseDatabase.getInstance();

        String pos;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            pgmName = bundle.getString("PgmName");

            PID = bundle.getString("pgmId");
        }

        textView.setText(pgmName);
        final DatabaseReference databaseReferenceTac;


        databaseReferenceTac = firebaseDatabase.getReferenceFromUrl("https://think-tac-india.firebaseio.com/programs/" + PID + "/tactivities");

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);

        recycler_item_models_main_list = new ArrayList<>();

        databaseReferenceTac.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                Recycler_item_model recycler_item_model = dataSnapshot.getValue(Recycler_item_model.class);

                recycler_item_models_main_list.add(recycler_item_model);

                adapter = new RecyclerAdapter(recycler_item_models_main_list, getApplicationContext());

                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {


            }
        });


    }
}