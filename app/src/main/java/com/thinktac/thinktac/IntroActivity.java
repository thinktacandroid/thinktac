package com.thinktac.thinktac;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class IntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);


        addSlide(AppIntroFragment.newInstance("Fun and Engaging","TACtivities are fun and engaging." +
                        " Coupled with the unique way of introducing concepts and linkages to real life examples," +
                        " Science ceases to be dull and boring",
                R.drawable.ispf_community, ContextCompat.getColor(getApplicationContext(),
                        R.color.colorPrimary)));

        addSlide(AppIntroFragment.newInstance("Games",
                        "Games designed for a better understanding the TACtivity and the related scientific topics are available for most topics." +
                        " The games are designed as an exploration with subsequent levels dwelling deeper into the topic.\n",
                R.drawable.thinktac_joystick,
                ContextCompat.getColor(getApplicationContext(),R.color.colorPrimary)));

    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}
