package com.thinktac.thinktac.model;

import android.os.Bundle;
import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

@Keep
public class Program{
    public String pid;

    public Program(String pid, String name, String image) {
        this.pid = pid;
        this.name = name;
        this.image = image;
    }

    public String name;
   public String image;


    public Program() {
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
