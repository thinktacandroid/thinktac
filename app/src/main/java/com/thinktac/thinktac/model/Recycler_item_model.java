package com.thinktac.thinktac.model;

import android.support.annotation.Keep;

@Keep
public class Recycler_item_model {
    private  String TID;
    public String name;
    public int status;

    public Recycler_item_model() {
    }

    public String getTID() {
        return this.TID;
    }

    public void setTID(String TID) {
        this.TID = TID;
    }

    public Recycler_item_model(String name, int status, String TID) {
        this.name = name;
        this.status = status;
        this.TID = TID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
