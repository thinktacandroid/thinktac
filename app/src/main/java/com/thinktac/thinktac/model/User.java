package com.thinktac.thinktac.model;

import android.support.annotation.Keep;

@Keep
public class User {

    public String firstname;
    public String email;
    private String DOB;
    private String MobileNo;
    public Boolean imageSet;

    public User(String firstname, String email, String DOB, String mobileNo, Boolean imageSet) {
        this.firstname = firstname;
        this.email = email;
        this.DOB = DOB;
        MobileNo = mobileNo;
        this.imageSet = imageSet;
    }

    public Boolean getImageSet() {
        return imageSet;
    }

    public void setImageSet(Boolean imageSet) {
        this.imageSet = imageSet;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public User() {

    }
}
