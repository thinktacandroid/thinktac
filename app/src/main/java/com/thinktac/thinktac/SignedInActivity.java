package com.thinktac.thinktac;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.logging.Handler;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignedInActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener{

    private ProgressBar spinner;
    BottomNavigationView bottomNavigationView;
    boolean doubleBackToExitPressedOnce = false;
    private FragmentManager fragmentManager;
    private HomeFragment homeFragment;
    private ProfileFragment profileFragment;
    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signed_in);

        checkConnection();

        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.VISIBLE);
        bottomNavigationView = findViewById(R.id.bottomNavView);
        fragmentManager = getSupportFragmentManager();
        replaceByFragmentHome();
        fragmentManager.addOnBackStackChangedListener(this);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu:
                        replaceByFragmentHome();
                        fragmentManager.popBackStack("replaceHome",0);
                        spinner.setVisibility(View.INVISIBLE);
                        break;

                    case R.id.profile_menu:
                        replaceByFragmentProfile();
                        break;
                }
                return true;
            }


        });



    }


    private void replaceByFragmentHome(){
        homeFragment = new HomeFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.containerLayout,homeFragment,"homeFragment");

        bottomNavigationView.getMenu().getItem(0).setChecked(true);
        fragmentTransaction.commit();

    }

    private void replaceByFragmentProfile(){
        profileFragment = new ProfileFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.containerLayout,profileFragment,"profileFragment");

        bottomNavigationView.getMenu().getItem(0).setChecked(true);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {

       if (bottomNavigationView.getSelectedItemId() == R.id.home_menu)
       {
           new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                            finish();

                    }
                })
                .setNegativeButton("No", null)
                .show();
       }
       else
       {
           replaceByFragmentHome();
       }
    }


    @Override
    public void onBackStackChanged() {

    }




    protected boolean isOnline() {

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnectedOrConnecting()) {

            return true;

        } else {

            return false;

        }

    }

    public void checkConnection(){

        if(isOnline()){

            Toast.makeText(getApplicationContext(), "Welcome!", Toast.LENGTH_SHORT).show();

        }else{

            Toast.makeText(getApplicationContext(), "Please check the internet connection!", Toast.LENGTH_SHORT).show();

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

    }


}
